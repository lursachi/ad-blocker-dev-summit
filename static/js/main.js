"use strict";

(function() {
        function navigationClick(event) {
            document.getElementById("nav-links")
                .classList.toggle("visible");
            event.preventDefault();
        }

        function initMenu() {
            document.getElementById("navbar-menu-toggle")
                .addEventListener("click", navigationClick, false);
        }

        // smooth scrolling navbar links
        function initSmoothScroll() {
            function scrollTo(element, to, duration) {
                if (duration <= 0) return;
                var difference = to - element.scrollTop;
                var perTick = difference / duration * 10;

                setTimeout(function () {
                    element.scrollTop = element.scrollTop + perTick;
                    if (element.scrollTop === to) return;
                    scrollTo(element, to, duration - 10);
                }, 10);
            }

            document.querySelectorAll("#nav-menu a").forEach(function (anchorLink) {
                anchorLink.addEventListener("click", function (e) {
                    //for non JS - you're welcome, Julian
                    e.preventDefault();
                    var anchorTarget = document.getElementById(anchorLink.dataset.target);
                    scrollTo(document.scrollingElement, anchorTarget.offsetTop, 500);
                    document.getElementById("nav-links").classList.remove("visible");
                });
            });
        }

        // make each menu link active when target section is in viewport
        function initActiveNavLink() {
            window.addEventListener("scroll", function () {
                var contactLink = document.querySelector('a[data-target="contact"]');
                var whyJoinLink = document.querySelector('a[data-target="why-join"]');
                var topicsLink = document.querySelector('a[data-target="topics"]');

                contactLink.classList.remove("active");
                whyJoinLink.classList.remove("active");
                topicsLink.classList.remove("active");

                var scrollTop = document.scrollingElement.scrollTop;
                var scrollBottom = scrollTop + window.innerHeight;
                var contact = document.getElementById("contact");
                var whyJoin = document.getElementById("why-join");
                var topics = document.getElementById("topics");

                if (scrollTop > contact.offsetTop - 100 || scrollBottom >= contact.offsetTop + contact.offsetHeight) {
                    contactLink.classList.add("active");
                } else if (scrollTop > topics.offsetTop - 100) {
                    topicsLink.classList.add("active");
                } else if (scrollTop > whyJoin.offsetTop - 100) {
                    whyJoinLink.classList.add("active");
                }
            });
        }
    if (document.getElementById("nav-links")) {
        initMenu();
        initSmoothScroll();
        initActiveNavLink();
    }
})();